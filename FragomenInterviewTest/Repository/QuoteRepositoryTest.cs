﻿using FragomenInterview.Model;
using FragomenInterview.Repository;
using FragomenInterview.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace FragomenInterviewTest.Repository
{
    [TestClass]
    public class QuoteRepositoryTest
    {
        private Mock<IQuoteService> _repoService;
        private QuoteRepository _uut;


        [TestInitialize]
        public void SetUp()
        {
            _repoService = new Mock<IQuoteService>();
            _uut = new QuoteRepository(_repoService.Object);
        }

        [TestMethod]
        public void FindPairs_PairsExist_ReturnMatchingPairs()
        {
            //Arrange

            var quotesList = new List<Quote>();
            quotesList.Add(new Quote
            {
                Author = "Brad",
                Id = 0,
                Text = "Quote1"
            });

            quotesList.Add(new Quote
            {
                Author = "Ezgi",
                Id = 1,
                Text = "Quote2"
            });

            quotesList.Add(new Quote
            {
                Author = "Esin",
                Id = 1,
                Text = "TestingQuote3"
            });

            quotesList.Add(new Quote
            {
                Author = "Cihat",
                Id = 1,
                Text = "TestingQuote4"
            });

            _repoService.Setup(q => q.GetAll()).Returns(quotesList);

            //Act
            var result = _uut.FindPairs(12);

            //Assert
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void FindPairs_PairsDoNotExist_ReturnMatchingPairs()
        {
            //Arrange
            _repoService.Setup(q => q.GetAll()).Returns(new List<Quote>());

            //Act
            var result = _uut.FindPairs(12);

            //Assert
            Assert.AreEqual(0, result);
        }
    }
}
